"use strict";

$(document).ready(function () {
    $('#nav-icon').click(function () {
        $(this).toggleClass('open');
        $('.order').toggleClass('active-nav');
    });

    $('.ingle-item').slick({
        dots: true,
        infinite: true,
        speed: 500,
        autoplay:true,
        autoplaySpeed:5000,
        fade: true,
        cssEase: 'linear'
    });

    $('.single-item').slick({
        infinite: true,
        speed: 500,
        autoplay:false,
        fade: true,
        cssEase: 'linear',
        prevArrow:'<svg class="photogallery__next" width="45pt" height="156pt" viewBox="0 0 45 156" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="#383838fe"><path  opacity="1.00" d=" M 41.64 0.00 L 41.78 0.00 C 42.39 0.35 43.61 1.06 44.22 1.41 C 30.85 27.01 17.03 52.38 3.51 77.91 C 17.34 103.44 31.14 128.99 44.99 154.51 C 44.39 154.88 43.20 155.63 42.61 156.00 L 42.47 156.00 C 28.09 130.17 14.52 103.88 0.00 78.14 L 0.00 78.12 C 13.87 52.08 27.87 26.10 41.64 0.00 Z" /></g></svg>',
        nextArrow:'<svg class="photogallery__prev" width="46pt" height="156pt" viewBox="0 0 46 156" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="#6ba422fe"><path opacity="1.00" d=" M 0.72 1.76 C 1.36 1.40 2.65 0.66 3.29 0.30 C 17.30 26.20 31.26 52.14 45.19 78.10 C 31.01 104.06 16.84 130.04 2.64 156.00 L 2.55 156.00 C 1.92 155.63 0.65 154.88 0.01 154.51 C 13.94 129.03 27.88 103.55 41.75 78.04 C 28.12 52.59 14.37 27.20 0.72 1.76 Z" /></g></svg>',
        
    });

    
});


    /* ===== Logic for creating fake Select Boxes ===== */
$('.sel').each(function() {
    $(this).children('select').css('display', 'none');
    
    var $current = $(this);
    
    $(this).find('option').each(function(i) {
      if (i == 0) {
        $current.prepend($('<div>', {
          class: $current.attr('class').replace(/sel/g, 'sel__box')
        }));
        
        var placeholder = $(this).text();
        $current.prepend($('<span>', {
          class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
          text: placeholder,
          'data-placeholder': placeholder
        }));
        
        return;
      }
      
      $current.children('div').append($('<span>', {
        class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
        text: $(this).text()
      }));
    });
  });
  
  // Toggling the `.active` state on the `.sel`.
  $('.sel').click(function() {
    $(this).toggleClass('active');
  });
  
  // Toggling the `.selected` state on the options.
  $('.sel__box__options').click(function() {
    var txt = $(this).text();
    var index = $(this).index();
    
    $(this).siblings('.sel__box__options').removeClass('selected');
    $(this).addClass('selected');
    
    var $currentSel = $(this).closest('.sel');
    $currentSel.children('.sel__placeholder').text(txt);
    $currentSel.children('select').prop('selectedIndex', index + 1);
  });